//
//  LoginViewController.swift
//  PalmTree
//
//  Created by Zeinab Reda on 12/29/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideKeyboard()
    }


    @IBAction func loginBtnAction(_ sender: Any) {
        
        if usernameTF.text == ""
        {
            Helper.showFloatAlert(title: "user name is empty", subTitle: "", type: Constants.AlertType.AlertError)
        }
        else if passwordTF.text == ""
        {
            Helper.showFloatAlert(title: "password is empty", subTitle: "", type: Constants.AlertType.AlertError)

        }
        else
        {
            if usernameTF.text != "admin" || passwordTF.text != "admin"
            {
                Helper.showAlert(type: Constants.AlertType.AlertError, title: "Not Authorized User", subTitle: "wrong user name or password", closeTitle: "close")
            }
            else
            {
                Helper.saveUserDefault(key: Constants.userDefault.userData, value: "admin")
                let nv = self.storyboard?.instantiateViewController(withIdentifier: "mainNV")
                self.present(nv!, animated: true, completion: nil)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  
}
