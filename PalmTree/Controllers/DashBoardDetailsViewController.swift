//
//  DashBoardDetailsViewController.swift
//  PalmTree
//
//  Created by Zeinab Reda on 12/29/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import SwiftCharts
import MapKit
class CustomPointAnnotation: MKPointAnnotation {
    
    var pinCustomImageName: String!
}
class DashBoardDetailsViewController: UIViewController {
    
    @IBOutlet weak var dashBoardTB: UITableView!
    var popups: [UIView] = []
    var treeID:String?
    var treeData:Row?
    var treeDate:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        ServiceManager().getInfectedTrees(treeName: treeID!){ [weak self](response, error) in
            
            if error == nil
            {
                if (response?.rows?.count)! > 0
                {
                    self?.treeData = response?.rows![0]
                    self?.dashBoardTB.reloadData()
                }
                else
                {
                    Helper.showFloatAlert(title: "No Propreties Found", subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                
            }
            else
            {
                Helper.showFloatAlert(title: "Something went wrong , try again", subTitle: "", type: Constants.AlertType.AlertError)
            }
        }
    }

    public static func create() -> DashBoardDetailsViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! DashBoardDetailsViewController
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DashBoardDetailsViewController:UITableViewDelegate , UITableViewDataSource , ChartDelegate
{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "reminder_cell") as! InfectedTreeTableViewCell
            
            cell.reminderDate.text = "Last identified \(treeDate). Apply Medication immediately"

            return cell
        }
        else if indexPath.row == 1
        {
            
            
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "alert_location_cell") as! AlertLocationTableViewCell
            
           
            let location = CLLocationCoordinate2D(latitude: Double((treeData?.location?.latitude) ?? Double(0.0)), longitude: Double((treeData?.location?.longitude) ?? Double(0.0)))
            
            let region = MKCoordinateRegionMake(location, MKCoordinateSpan(latitudeDelta: 0.9, longitudeDelta: 0.9))
            cell.map.setRegion(region, animated: true)
            
            let pointAnnotation = CustomPointAnnotation()
            pointAnnotation.pinCustomImageName = "mosque_gps_icon"
            pointAnnotation.coordinate = location
            pointAnnotation.title = "location"

            
            let pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pin")
            cell.map.addAnnotation(pinAnnotationView.annotation!)
            return cell
        }
        else if indexPath.row == 2
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "chart_cell")
            drawCell(cellView: (cell?.contentView)!, color: UIColor.blue, xTitle: "Water Level", yTitle: "")
            return cell!
      
        }
        
        else if indexPath.row == 3
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "chart_cell")
            drawCell(cellView: (cell?.contentView)!, color: UIColor.green, xTitle: "Soil Quality", yTitle: "")
            return cell!
            
        }
        
        else if indexPath.row == 4
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "chart_cell")
            drawCell(cellView: (cell?.contentView)!, color: UIColor.red, xTitle: "Temperature", yTitle: "")
            return cell!
            
        }
        
        else if indexPath.row == 5
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "chart_cell")
            drawCell(cellView: (cell?.contentView)!, color: UIColor.yellow, xTitle: "Humidity", yTitle: "")
            return cell!
            
        }
        else if indexPath.row == 6
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "chart_cell")
            drawCell(cellView: (cell?.contentView)!, color: UIColor.cyan, xTitle: "Wind Speed (mph)", yTitle: "")
            return cell!
            
        }
        
        else
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "chart_cell")
            drawCell(cellView: (cell?.contentView)!, color: UIColor.brown, xTitle: "Sunlight Levels", yTitle: "")
            return cell!
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0
        {
            return 140.0
        }
        else if indexPath.row == 1
        {
            return 240.0

        }
        else
        {
            return 300.0

        }
    }
    
    
    
    func drawCell(cellView:UIView , color:UIColor , xTitle:String , yTitle:String)
    {
        cellView.subviews.forEach { $0.removeFromSuperview() }

         var chart: Chart? // arc
        
        let labelSettings = ChartLabelSettings(font: ExamplesDefaults.labelFont)
        
        //        let chartPoints1 = [(0, 50), (2, 65), (4, 125), (6, 140)].map{ChartPoint(x: ChartAxisValueInt($0.0, labelSettings: labelSettings), y: ChartAxisValueInt($0.1))}
        //        let chartPoints2 = [(0, 150), (2, 100), (4, 200), (6, 60)].map{ChartPoint(x: ChartAxisValueInt($0.0, labelSettings: labelSettings), y: ChartAxisValueInt($0.1))}
        let chartPoints3 = [(0, 200), (2, 210), (4, 260), (6, 290)].map{ChartPoint(x: ChartAxisValueInt($0.0, labelSettings: labelSettings), y: ChartAxisValueInt($0.1))}
        
        let allChartPoints = ( chartPoints3).sorted {(obj1, obj2) in return obj1.x.scalar < obj2.x.scalar}
        
        let xValues: [ChartAxisValue] = (NSOrderedSet(array: allChartPoints).array as! [ChartPoint]).map{$0.x}
        let yValues = ChartAxisValuesStaticGenerator.generateYAxisValuesWithChartPoints(allChartPoints, minSegmentCount: 5, maxSegmentCount: 20, multiple: 50, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: labelSettings)}, addPaddingSegmentIfEdge: false)
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: xTitle, settings: labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: yTitle, settings: labelSettings.defaultVertical()))
        let chartFrame = ExamplesDefaults.chartFrame(cellView.bounds)
        var chartSettings = ExamplesDefaults.chartSettingsWithPanZoom
        chartSettings.trailing = 20
        chartSettings.labelsToAxisSpacingX = 20
        chartSettings.labelsToAxisSpacingY = 20
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxisLayer, yAxisLayer, innerFrame) = (coordsSpace.xAxisLayer, coordsSpace.yAxisLayer, coordsSpace.chartInnerFrame)
        
//        let c1 = UIColor.blue
//        let c2 = UIColor.red
    //    let c3 = UIColor.green
        
        //        let lineModel1 = ChartLineModel(chartPoints: chartPoints1, lineColor: UIColor.black, animDuration: 1, animDelay: 0)
        //        let lineModel2 = ChartLineModel(chartPoints: chartPoints2, lineColor: UIColor.black, animDuration: 1, animDelay: 0)
        let lineModel3 = ChartLineModel(chartPoints: chartPoints3, lineColor: UIColor.black, animDuration: 1, animDelay: 0)
        
        let chartPointsLineLayer = ChartPointsLineLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, lineModels: [ lineModel3], pathGenerator: CatmullPathGenerator())
        
        //        let chartPointsLayer1 = ChartPointsAreaLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, chartPoints: chartPoints1, areaColors: [c1], animDuration: 3, animDelay: 0, addContainerPoints: true, pathGenerator: chartPointsLineLayer.pathGenerator)
        //        let chartPointsLayer2 = ChartPointsAreaLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, chartPoints: chartPoints2, areaColors: [c2], animDuration: 3, animDelay: 0, addContainerPoints: true, pathGenerator: chartPointsLineLayer.pathGenerator)
//        let chartPointsLayer3 = ChartPointsAreaLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, chartPoints: chartPoints3, areaColors: [c3], animDuration: 3, animDelay: 0, addContainerPoints: true, pathGenerator: chartPointsLineLayer.pathGenerator)
        
        let chartPointsLayer3 = ChartPointsAreaLayer(xAxis:  xAxisLayer.axis, yAxis: yAxisLayer.axis, chartPoints: chartPoints3, areaColor: color, animDuration: 3.0, animDelay: 0, addContainerPoints: true)
        
        var selectedView: ChartPointTextCircleView?
        
        let circleViewGenerator = {[weak self] (chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in guard let weakSelf = self else {return nil}
            
            let (chartPoint, screenLoc) = (chartPointModel.chartPoint, chartPointModel.screenLoc)
            
            let v = ChartPointTextCircleView(chartPoint: chartPoint, center: screenLoc, diameter: Env.iPad ? 50 : 30, cornerRadius: Env.iPad ? 24: 15, borderWidth: Env.iPad ? 2 : 1, font: ExamplesDefaults.fontWithSize(Env.iPad ? 14 : 8))
            v.viewTapped = {view in
                for p in weakSelf.popups {p.removeFromSuperview()}
                selectedView?.selected = false
                
                let w: CGFloat = Env.iPad ? 250 : 150
                let h: CGFloat = Env.iPad ? 100 : 80
                
                if let chartViewScreenLoc = layer.containerToGlobalScreenLoc(chartPoint) {
                    let x: CGFloat = {
                        let attempt = chartViewScreenLoc.x - (w/2)
                        let leftBound: CGFloat = chart.bounds.origin.x
                        let rightBound = chart.bounds.size.width - 5
                        if attempt < leftBound {
                            return view.frame.origin.x
                        } else if attempt + w > rightBound {
                            return rightBound - w
                        }
                        return attempt
                    }()
                    
                    let frame = CGRect(x: x, y: chartViewScreenLoc.y - (h + (Env.iPad ? 30 : 12)), width: w, height: h)
                    
                    let bubbleView = InfoBubble(point: chartViewScreenLoc, frame: frame, arrowWidth: Env.iPad ? 40 : 28, arrowHeight: Env.iPad ? 20 : 14, bgColor: UIColor.black, arrowX: chartViewScreenLoc.x - x, arrowY: -1) // TODO don't calculate this here
                    chart.view.addSubview(bubbleView)
                    
                    bubbleView.transform = CGAffineTransform(scaleX: 0, y: 0).concatenating(CGAffineTransform(translationX: 0, y: 100))
                    let infoView = UILabel(frame: CGRect(x: 0, y: 10, width: w, height: h - 30))
                    infoView.textColor = UIColor.white
                    infoView.backgroundColor = UIColor.black
                    infoView.text = "Some text about \(chartPoint)"
                    infoView.font = ExamplesDefaults.fontWithSize(Env.iPad ? 14 : 12)
                    infoView.textAlignment = NSTextAlignment.center
                    
                    bubbleView.addSubview(infoView)
                    weakSelf.popups.append(bubbleView)
                    
                    UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
                        view.selected = true
                        selectedView = view
                        
                        bubbleView.transform = CGAffineTransform.identity
                    }, completion: {finished in})
                }
            }
            
            UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
                let w: CGFloat = v.frame.size.width
                let h: CGFloat = v.frame.size.height
                let frame = CGRect(x: screenLoc.x - (w/2), y: screenLoc.y - (h/2), width: w, height: h)
                v.frame = frame
            }, completion: nil)
            
            return v
        }
        
        let itemsDelay: Float = 0.08
        
   
        //
        let chartPointsCircleLayer3 = ChartPointsViewsLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, chartPoints: chartPoints3, viewGenerator: circleViewGenerator, displayDelay: 2.7, delayBetweenItems: itemsDelay, mode: .translate)
        
        let settings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.black, linesWidth: ExamplesDefaults.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesDottedLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: settings)
        
         chart = Chart(
            frame: chartFrame,
            innerFrame: innerFrame,
            settings: chartSettings,
            layers: [
                xAxisLayer,
                yAxisLayer,
                guidelinesLayer,
                //                chartPointsLayer1,
                //                chartPointsLayer2,
                chartPointsLayer3,
                chartPointsLineLayer,
                //                chartPointsCircleLayer1,
                //                chartPointsCircleLayer2,
                chartPointsCircleLayer3
            ]
        )
        
        chart?.delegate = self
        
        cellView.addSubview((chart?.view)!)

    }
    
    
    fileprivate func removePopups() {
        for popup in popups {
            popup.removeFromSuperview()
        }
    }
    // MARK: - ChartDelegate
    
    func onZoom(scaleX: CGFloat, scaleY: CGFloat, deltaX: CGFloat, deltaY: CGFloat, centerX: CGFloat, centerY: CGFloat, isGesture: Bool) {
        removePopups()
    }
    
    func onPan(transX: CGFloat, transY: CGFloat, deltaX: CGFloat, deltaY: CGFloat, isGesture: Bool, isDeceleration: Bool) {
        removePopups()
    }
    
    func onTap(_ models: [TappedChartPointLayerModels<ChartPoint>]) {
    }
    
}
