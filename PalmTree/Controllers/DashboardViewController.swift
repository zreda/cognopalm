//
//  DashboardViewController.swift
//  PalmTree
//
//  Created by Zeinab Reda on 12/29/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import Charts
import KDCircularProgress

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var dashBoardTB: UITableView!
    var label = UILabel()
    var lineChart: LineChart!
    var treeData:Row?
    var dashboardRow:Int = 0
    var localDate:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dashboradTimer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(DashboardViewController.getDashboardData), userInfo: nil, repeats: true)
        getDashboardData()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func getDashboardData()
    {
        ServiceManager().getInfectedTrees { [weak self](response, error) in
            
            if error == nil
            {
                if (response?.rows?.count)! > 0
                {
                    self?.treeData = response?.rows![0]
                    self?.dashboardRow = 4
                    self?.dashBoardTB.reloadData()
                }
                else
                {
                    Helper.showFloatAlert(title: "No Propreties Found", subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                
            }
            else
            {
                Helper.showFloatAlert(title: "Something went wrong , try again", subTitle: "", type: Constants.AlertType.AlertError)
            }
        }
    }
    
    public static func create() -> DashboardViewController {
        
        return UIStoryboard(name: Constants.StoryBoard.mainSB, bundle: Bundle.main).instantiateViewController(withIdentifier: String(describing: self)) as! DashboardViewController
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension DashboardViewController :UITableViewDelegate , UITableViewDataSource , LineChartDelegate
{
    
    func generateDataEntries() -> [BarEntry] {
        let colors = [#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1), #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1), #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)]
        var result: [BarEntry] = []
        var j = 4
        var months = ["1 July","30 Agust" ,"12 Dec" ,"31 Dec"]
        for i in 0..<4 {
            let value = (arc4random() % 90) + 10
            let height: Float = Float(value) / 100.0
            let formatter = DateFormatter()
            formatter.dateFormat = "d MMM"
            var date = Date()
            date.addTimeInterval(TimeInterval(24*60*60*0))
            j = j+1
            result.append(BarEntry(color: colors[i % colors.count], height: height, textValue: "\(value)", title: months[i]))
        }
        return result
    }
    
    
    func setChart(cell:BieChartTableViewCell,dataPoints: [String], values: [Double]) {

        
        let dataEntries = generateDataEntries()
        
        if (treeData?.totalNumberofTrees)! > 0
        {
            let percentage: Int = Int(Float((treeData?.numberofInfestedTrees)!) / Float((treeData?.totalNumberofTrees)!) * 100)
                
            cell.percentgeLB.text  = "\(percentage) %"
            cell.pieChartView.angle = Double(percentage)
            let percentAngel: Double = Double(360*percentage)/100.0
            cell.pieChartView.animate(toAngle: percentAngel, duration: 1, completion: nil)
        }
        else
        {
            cell.percentgeLB.text = "0 %"
            
            cell.pieChartView.animate(toAngle: 0, duration: 1, completion: nil)

        }
        cell.lineChartView.dataEntries = dataEntries

        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "infected_trees_cell") as! DashboradHeaderTableViewCell
            cell.infectedTress.text = "There are " + String(describing: treeData?.totalNumberofTrees as! Int) + " infected trees. "
            
            //            cell.lastTimeInfection.text = "Last identified "+String(describing: treeData?.infestationtime as! Int)
            
//            
//            let date = NSDate(timeIntervalSince1970: TimeInterval(treeData?.infestationtime as! Int))
//            
//            let dayTimePeriodFormatter = DateFormatter()
//            dayTimePeriodFormatter.dateFormat = "dd MMM YYYY hh:mm a"
//            
//            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
//            let treeDate = Double(treeData?.infestationtime as! Int).getDateStringFromUTC()
            
            
//            let treeDate = Double(treeData?.infestationtime as! Int).getDateStringFromUnixTime(dateStyle: DateFormatter.Style.medium, timeStyle: DateFormatter.Style.medium)
            let treeDate = "28 Dec, 2017 1:00:00 AM"
            
            self.localDate = treeDate
            
            
            if (treeData?.numberofInfestedTrees)! > 0 {
                cell.lastTimeInfection.text = "Last identified " + treeDate
                cell.lastTimeInfection.isHidden = false
                
            }
            else
            {
                cell.lastTimeInfection.isHidden = true
            }
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "line_chart_cell") as! LineChartTableViewCell
            var views: [String: AnyObject] = [:]
            
            // simple arrays
            let data: [CGFloat] = [3, 4, -2, 11, 13, 15]
            let data2: [CGFloat] = [1, 3, 5, 13, 17, 20]
            
            // simple line with custom x axis labels
            let xLabels: [String] = ["May","Jun", "July", "Augst", "Nov", "Dec"]
            //            let yLabels: [String] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
            lineChart = LineChart()
            lineChart.animation.enabled = true
            lineChart.area = true
            lineChart.x.labels.visible = true
            lineChart.x.grid.count = 6
            lineChart.y.grid.count = 6
            lineChart.x.labels.values = xLabels
            lineChart.y.labels.visible = true
            lineChart.addLine(data)
            lineChart.addLine(data2)
            
            lineChart.translatesAutoresizingMaskIntoConstraints = false
            lineChart.delegate = self
            cell.bgView.addSubview(lineChart)
            views["chart"] = lineChart
            cell.bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[chart]-|", options: [], metrics: nil, views: views))
            cell.bgView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[chart]-|", options: [], metrics: nil, views: views))
            
            return cell
        }
            
        else if indexPath.row == 2
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "bie_chart_cell") as! BieChartTableViewCell
            
            cell.totalTressInfected.text = String(describing: treeData?.totalNumberofTrees as! Int)
            cell.todayTressInfected.text = String(describing: treeData?.numberofInfestedTrees as! Int)
            
            // Do any additional setup after loading the view.
            
            let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun"]
            let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0]
            
            setChart(cell: cell, dataPoints: months, values: unitsSold)
            return cell
        }
        else if indexPath.row == 3
        {
            let cell = dashBoardTB.dequeueReusableCell(withIdentifier: "alert_cell") as! AlertTableViewCell
            if (treeData?.numberofInfestedTrees)! > 0 {
                cell.totalInspectedTrees.isHidden = false
                cell.inspectBtn.isHidden = false
                cell.totalInspectedTrees.text = "Tree " + (treeData?.infestedTreeIDs!)! + " is infected"
                cell.inspectBtn.addTarget(self, action: #selector(alertBtnTapped), for: UIControlEvents.touchUpInside)
                
            }
            else
                
            {
                cell.totalInspectedTrees.isHidden = true
                cell.inspectBtn.isHidden = true
            }
            cell.inspectBtn.tag = indexPath.row
            return cell
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0
        {
            return 140.0
        }
        else if indexPath.row == 1
        {
            return 240.0
        }
        else if indexPath.row == 2
        {
            return 300.0
        }
        else if indexPath.row == 3
        {
            return 200.0
        }
        return 60
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dashboardRow
    }
    
    func alertBtnTapped(sender: UIButton){
        //        let buttonTag = sender.tag
        let vc = DashBoardDetailsViewController.create()
        vc.treeID = treeData?.infestedTreeIDs!
        vc.treeDate = localDate
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    /**
     * Line chart delegate method.
     */
    func didSelectDataPoint(_ x: CGFloat, yValues: Array<CGFloat>) {
        label.text = "x: \(x)     y: \(yValues)"
    }
    
    
    
    /**
     * Redraw chart on device rotation.
     */
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        if let chart = lineChart {
            chart.setNeedsDisplay()
        }
    }
    
}
