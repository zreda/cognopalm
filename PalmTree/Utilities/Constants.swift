
import UIKit

class Constants: NSObject {
    
//    PalmTree
    
    static let main_url:String = "http://13.66.59.32:1877/Thingworx/Things/%@/Properties?appKey=cf3ac4df-8f8a-4af3-8b22-0cac7219658c"
    

    

    struct StatusCode {
        
        static let StatusOK = 200
        static let StatusBadRequest = 400
        static let StatusNotfound = 404
        static let UserNotAuthorized = 401
        static let UserForbidden = 403
        static let Failure = 500
        static let Unavailable = 503
    }
    
 
    
    struct AlertType {
        static let AlertSuccess = 2
        static let AlertError = 1
        static let Alertinfo = 3
        static let AlertWarn = 4
    }
    
    struct userDefault {
        static let userData = "userData"
    }
    
    struct StoryBoard {
        static let mainSB = "Main"
        
    }
    
    
}
