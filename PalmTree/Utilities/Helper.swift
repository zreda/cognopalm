//
//  Helper.swift
//  HubMe
//
//  Created by Zeinab Reda on 11/15/16.
//

import UIKit
import Foundation
import SCLAlertView
import JDropDownAlert
import KDCircularProgress
extension Double {
    func getDateStringFromUTC() -> String {
        let date = Date(timeIntervalSince1970: self)
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateStyle = .medium
        
        return dateFormatter.string(from: date)
    }
    
    func getDateStringFromUnixTime(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = dateStyle
        dateFormatter.timeStyle = timeStyle
        return dateFormatter.string(from: Date(timeIntervalSince1970: self))
    }
}

extension UIView {
    
    static func setupCircularProgress(progress: KDCircularProgress, percent: Double, fillColor: UIColor) {
        
        progress.startAngle = -90
        progress.progressThickness = 0.35
        progress.trackThickness = 0.25
        progress.trackColor = UIColor.white
        progress.glowMode = .forward
        progress.glowAmount = 0.5
        progress.set(colors: fillColor)
        
        let percentAngel:Double = ((360*percent)/100)
        progress.animate(toAngle: percentAngel, duration: 1, completion: nil)
    }
    
}


public extension UIImage
{
    func base64Encode() -> String?
    {
        guard let imageData = UIImagePNGRepresentation(self) else
        {
            return nil
        }
        
        let base64String = imageData.base64EncodedString()
        let fullBase64String = "data:image/png;base64,\(base64String))"
        
        return fullBase64String
    }
}

extension String.CharacterView {
    /// This method makes it easier extract a substring by character index where a character is viewed as a human-readable character (grapheme cluster).
    internal func substring(start: Int, offsetBy: Int) -> String? {
        guard let substringStartIndex = self.index(startIndex, offsetBy: start, limitedBy: endIndex) else {
            return nil
        }
        
        guard let substringEndIndex = self.index(startIndex, offsetBy: start + offsetBy, limitedBy: endIndex) else {
            return nil
        }
        
        return String(self[substringStartIndex ..< substringEndIndex])
    }
}


extension UILabel{
    
    func underLine(){
        if let textUnwrapped = self.text{
            let underlineAttribute = [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue]
            let underlineAttributedString = NSAttributedString(string: textUnwrapped, attributes: underlineAttribute)
            self.attributedText = underlineAttributedString
        }
    }
}

extension UIAlertController {
    func changeFont(view:UIView,font:UIFont) {
        for item in view.subviews {
            if item is UICollectionView {
                let col = item as! UICollectionView
                for  row in col.subviews{
                    self.changeFont(view: row, font: font)
                }
            }
            if item is UILabel {
                let label = item as! UILabel
                label.font = font
            }else {
                self.changeFont(view: item, font: font)
            }
            
        }
    }
    open override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //        let font =  UIFont(name: "GE SS", size: 16.0)!
        //        changeFont(view: self.view, font: font )
    }
}

class Helper: NSObject {
    
    static let userDef = UserDefaults.standard
    
    static func convertDataToJson(data:Data) ->Any
    {
        var jsonDic : Any?
        do {
            jsonDic = try? JSONSerialization.jsonObject(with: data as Data, options: JSONSerialization.ReadingOptions())
        }
        return jsonDic as Any
    }
    
    static func rateApp(appId: String, completion: @escaping ((_ success: Bool)->()))
    {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
                completion(UIApplication.shared.openURL(url))
            return
    }
    UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
   
    
    static func getDayOfWeek(_ today:String) -> String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        
        switch weekDay {
        case 1:
            return "Sunday"
        case 2:
            return "Monday"
        case 3:
            return "Tueday"
        case 4:
            return "Wedday"
        case 5:
            return "Thuday"
        case 6:
            return "Friday"
        case 7:
            return "Saturday"
        default:
            print("Error fetching days")
            return "Day"
        }
        
     
    }
 
    
    
    // show custom alert
    static func showAlert(type:Int,title:String,subTitle:String , closeTitle:String)
    {
        if type == Constants.AlertType.AlertError
        {
            SCLAlertView().showError(title, subTitle: subTitle, closeButtonTitle:closeTitle)
        }
        else if type == Constants.AlertType.AlertSuccess
        {
            SCLAlertView().showSuccess(title, subTitle: subTitle , closeButtonTitle:closeTitle)
        }
        else if type == Constants.AlertType.Alertinfo
        {
            SCLAlertView().showInfo(title, subTitle: subTitle , closeButtonTitle:closeTitle)
        }
        else if type == Constants.AlertType.AlertWarn
        {
            SCLAlertView().showWarning(title, subTitle: subTitle , closeButtonTitle:closeTitle)
            
        }
    }
    
    static func showFloatAlert(title:String ,subTitle:String ,type:Int){
        
        let alert = JDropDownAlert()
        alert.alertWith(title)
        //        alert.titleFont = UIFont(name: "Helvetica", size: 20)!
        //        alert.messageFont = UIFont.italicSystemFont(ofSize: 12)
        
        if type == Constants.AlertType.AlertError
        {
            alert.alertWith(title, message: subTitle, topLabelColor: UIColor.white, messageLabelColor: UIColor.darkGray, backgroundColor: UIColor.red)
        }
        else if type == Constants.AlertType.AlertSuccess
        {
            alert.alertWith(title, message: subTitle, topLabelColor: UIColor.white, messageLabelColor: UIColor.darkGray, backgroundColor: UIColor.green)
            
        }
            
        else if type == Constants.AlertType.Alertinfo
        {
            alert.alertWith(title, message: subTitle, topLabelColor: UIColor.white, messageLabelColor: UIColor.darkGray, backgroundColor: UIColor.blue)
            
        }
        alert.didTapBlock = {
            debugPrint("Top View Did Tapped")
        }
    }
    
    
    // insert and retreive object or primitave data type in user defualt
    
    static func saveUserDefault(key:String,value:Any)
    {
        userDef.set(value, forKey: key)
    }
    static func getUserDefault(key:String)->Any
    {
        return userDef.object(forKey: key) as Any
    }
    static func removeKeyUserDefault(key:String)
    {
        return userDef.removeObject(forKey: key)
    }
    
    static func isKeyPresentInUserDefaults(key: String) -> Bool {
        return userDef.object(forKey: key) != nil
    }
    
    static func saveObjectDefault(key:String,value:Any)
    {
        let userDataEncoded = NSKeyedArchiver.archivedData(withRootObject: value)
        userDef.set(userDataEncoded, forKey: key)
        userDef.synchronize()
    }
    
    static func getObjectDefault(key:String)->Any
    {
        
        if let decodedNSData = UserDefaults.standard.object(forKey: key) as? NSData,
            let Data = NSKeyedUnarchiver.unarchiveObject(with: decodedNSData as Data)
        {
            
            return Data
        }
        else {
            debugPrint("Failed")
            
            return ""
        }
    }
    
    
    // convert UIColor from hexa color
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    // Animate Table View
    
    static func animateTable(table:UITableView) {
        table.reloadData()
        
        let cells = table.visibleCells
        let tableHeight: CGFloat = table.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1.5, delay:  0.05 * Double(index) , usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseIn, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            
            index += 1
        }
    }
    
    static func openUrl(urlStr:String)
    {
        var url = urlStr
        if !url.hasPrefix("http")
        {
            url = "http://"+urlStr
        }
        if let url = URL(string: url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            debugPrint("Open \(urlStr): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                debugPrint("Open \(urlStr): \(success)")
            }
        }

    
        
    }
    
    
     static func convertStringToDictionary(_ text: String) -> [String:String]? {
        
        if let data = text.data(using: String.Encoding.utf8) {
            
            do {
                
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:String]
                
                
                
            } catch let error as NSError {
                
                print(error)
                
            }
            
        }
        
        return nil
        
    }
    
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                // print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    static func getCurrentLang()-> String
    {
        if let lang =   Helper.getUserDefault(key: "lang") as? String
        {
            return lang
        }
        return "en"
    }
    
    static func getImageWithSize(urlStr:String,originalSize:String,updateSize:String)->String
    {//"20x14"
        let flag = urlStr.replacingOccurrences(of: originalSize, with: updateSize, options: .literal, range: nil)
        return flag
    }
    
    
    static func isValidEmail(mail_address:String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: mail_address)
        
    }
    
    static func isValidPhone(phone:String) -> Bool {
        
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phone.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  phone == filtered
        
    }
    
    static func getCountryCode(phone:String) -> String {
        
        var phoneNumber:String = phone
        if phone.characters.first == "0" {
            phoneNumber =  phone.replacingOccurrences(of: "0", with: "")
        }
        if phone.characters.first == "+" {
            phoneNumber =  phone.replacingOccurrences(of: "+", with: "")
        }
        
        let start = phoneNumber.index(phoneNumber.startIndex, offsetBy: phoneNumber.characters.count - 10)
        let end = phoneNumber.index(phoneNumber.endIndex, offsetBy: 0)
        let range = start..<end
        
        return phoneNumber.substring(with: range)
    }
    
    static func pushToViewController(stroyBoardName:String,controllerIdentifier:String,navController:UINavigationController)
    {
        let accountStoryBoard = UIStoryboard(name: stroyBoardName, bundle: nil)
        let controller = accountStoryBoard.instantiateViewController(withIdentifier: controllerIdentifier)
        navController.pushViewController(controller, animated: true)
    }
    
    static func presentViewController(stroyBoardName:String,controllerIdentifier:String,viewController:UIViewController)
    {
        let accountStoryBoard = UIStoryboard(name: stroyBoardName, bundle: nil)
        let controller = accountStoryBoard.instantiateViewController(withIdentifier: controllerIdentifier)
        viewController.present(controller, animated: true, completion: nil)
    }
    
    static func goToHomeController(controller:UIViewController)  {
        
        let tabBarController: UITabBarController = controller.storyboard?.instantiateViewController(withIdentifier: "HomeTabBar") as! UITabBarController
        tabBarController.selectedIndex = 2
        
        controller.navigationController?.present(tabBarController, animated: false, completion: nil)

    }
}

