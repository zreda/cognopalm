//
//  DesignableRoundedButton.swift
//  OrangeFootballClub
//
//  Created by Zeinab Reda on 2/26/17.
//  Copyright © 2017 Alaa Taher. All rights reserved.
//


import UIKit

@IBDesignable

class RoundedButton :UIButton {
    
    @IBInspectable override var borderWidth: CGFloat  {
        
        didSet {
            layer.borderWidth = borderWidth
        }
        
        
    }
    //Normal state bg and border
    @IBInspectable var normalBorderColor: UIColor? {
        didSet {
            layer.borderColor = normalBorderColor?.cgColor
        }
    }
    
    @IBInspectable var normalBackgroundColor: UIColor? {
        didSet {
            setBgColorForState(color: normalBackgroundColor, forState: .normal)
        }
    }
    
    
    //Highlighted state bg and border
    @IBInspectable var highlightedBorderColor: UIColor?
    
    @IBInspectable var highlightedBackgroundColor: UIColor? {
        didSet {
            setBgColorForState(color: highlightedBackgroundColor, forState: .highlighted)
        }
    }
    
    
    private func setBgColorForState(color: UIColor?, forState: UIControlState){
        if color != nil {
            setBackgroundImage(UIImage.imageWithColor(color: color!), for: forState)
            
        } else {
            setBackgroundImage(nil, for: forState)
        }
    }
    
    @IBInspectable var cornerRaduis: CGFloat = 1.0
    

    override func layoutSubviews() {
        super.layoutSubviews()
        
        if cornerRaduis != 0
        {
        layer.cornerRadius = layer.frame.height / cornerRaduis
    
        }
        else
        {
          layer.cornerRadius = 0
        }
        clipsToBounds = true
        
        if borderWidth > 0 {
            
            
            if state == .normal && (layer.borderColor != normalBorderColor?.cgColor) {
                layer.borderColor = normalBorderColor?.cgColor
            } else if state == .highlighted && highlightedBorderColor != nil{
                layer.borderColor = highlightedBorderColor!.cgColor
            }
        }
    }
    
}

//Extension Required by RoundedButton to create UIImage from UIColor
extension UIImage {
    class func imageWithColor(color: UIColor) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 1, height: 1), false, 1.0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
}
