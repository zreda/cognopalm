//
//  BieChartTableViewCell.swift
//  PalmTree
//
//  Created by Zeinab Reda on 12/29/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit
import Charts
import KDCircularProgress
class BieChartTableViewCell: UITableViewCell {
    @IBOutlet weak var pieChartView: KDCircularProgress!
    @IBOutlet weak var lineChartView: BasicBarChart!
    @IBOutlet weak var totalTressInfected: UILabel!
    @IBOutlet weak var todayTressInfected: UILabel!

    @IBOutlet weak var percentgeLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
