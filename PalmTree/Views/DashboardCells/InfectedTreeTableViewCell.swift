//
//  InfectedTreeTableViewCell.swift
//  PalmTree
//
//  Created by Zeinab Reda on 12/29/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class InfectedTreeTableViewCell: UITableViewCell {

    @IBOutlet weak var reminderDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
