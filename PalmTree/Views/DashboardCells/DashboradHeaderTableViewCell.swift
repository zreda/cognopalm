//
//  DashboradHeaderTableViewCell.swift
//  PalmTree
//
//  Created by Zeinab Reda on 12/30/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import UIKit

class DashboradHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var infectedTress: UILabel!
    @IBOutlet weak var lastTimeInfection: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
