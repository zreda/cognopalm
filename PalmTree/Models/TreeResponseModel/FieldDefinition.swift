//
//	FieldDefinition.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class FieldDefinition : NSObject, NSCoding, Mappable{

	var infestedTreeIDs : InfestedTreeID?
	var numberofInfestedTrees : InfestedTreeID?
	var totalNumberofTrees : InfestedTreeID?
	var descriptionField : InfestedTreeID?
	var infestationtime : InfestedTreeID?
	var infested : InfestedTreeID?
	var location : InfestedTreeID?
	var name : InfestedTreeID?
	var severity : InfestedTreeID?
	var tags : InfestedTreeID?
	var thingTemplate : InfestedTreeID?


	class func newInstance(map: Map) -> Mappable?{
		return FieldDefinition()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		infestedTreeIDs <- map["InfestedTreeIDs"]
		numberofInfestedTrees <- map["NumberofInfestedTrees"]
		totalNumberofTrees <- map["TotalNumberofTrees"]
		descriptionField <- map["description"]
		infestationtime <- map["infestationtime"]
		infested <- map["infested"]
		location <- map["location"]
		name <- map["name"]
		severity <- map["severity"]
		tags <- map["tags"]
		thingTemplate <- map["thingTemplate"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         infestedTreeIDs = aDecoder.decodeObject(forKey: "InfestedTreeIDs") as? InfestedTreeID
         numberofInfestedTrees = aDecoder.decodeObject(forKey: "NumberofInfestedTrees") as? InfestedTreeID
         totalNumberofTrees = aDecoder.decodeObject(forKey: "TotalNumberofTrees") as? InfestedTreeID
         descriptionField = aDecoder.decodeObject(forKey: "description") as? InfestedTreeID
         infestationtime = aDecoder.decodeObject(forKey: "infestationtime") as? InfestedTreeID
         infested = aDecoder.decodeObject(forKey: "infested") as? InfestedTreeID
         location = aDecoder.decodeObject(forKey: "location") as? InfestedTreeID
         name = aDecoder.decodeObject(forKey: "name") as? InfestedTreeID
         severity = aDecoder.decodeObject(forKey: "severity") as? InfestedTreeID
         tags = aDecoder.decodeObject(forKey: "tags") as? InfestedTreeID
         thingTemplate = aDecoder.decodeObject(forKey: "thingTemplate") as? InfestedTreeID

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if infestedTreeIDs != nil{
			aCoder.encode(infestedTreeIDs, forKey: "InfestedTreeIDs")
		}
		if numberofInfestedTrees != nil{
			aCoder.encode(numberofInfestedTrees, forKey: "NumberofInfestedTrees")
		}
		if totalNumberofTrees != nil{
			aCoder.encode(totalNumberofTrees, forKey: "TotalNumberofTrees")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if infestationtime != nil{
			aCoder.encode(infestationtime, forKey: "infestationtime")
		}
		if infested != nil{
			aCoder.encode(infested, forKey: "infested")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if severity != nil{
			aCoder.encode(severity, forKey: "severity")
		}
		if tags != nil{
			aCoder.encode(tags, forKey: "tags")
		}
		if thingTemplate != nil{
			aCoder.encode(thingTemplate, forKey: "thingTemplate")
		}

	}

}