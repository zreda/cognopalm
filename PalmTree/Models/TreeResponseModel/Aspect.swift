//
//	Aspect.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Aspect : NSObject, NSCoding, Mappable{

	var cacheTime : Int?
	var dataChangeType : String?
	var isLogged : Bool?
	var isPersistent : Bool?
	var isReadOnly : Bool?
	var dataChangeThreshold : Int?
	var isBuiltIn : Bool?
	var defaultValue : Int?
	var tagType : String?


	class func newInstance(map: Map) -> Mappable?{
		return Aspect()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		cacheTime <- map["cacheTime"]
		dataChangeType <- map["dataChangeType"]
		isLogged <- map["isLogged"]
		isPersistent <- map["isPersistent"]
		isReadOnly <- map["isReadOnly"]
		dataChangeThreshold <- map["dataChangeThreshold"]
		isBuiltIn <- map["isBuiltIn"]
		defaultValue <- map["defaultValue"]
		tagType <- map["tagType"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cacheTime = aDecoder.decodeObject(forKey: "cacheTime") as? Int
         dataChangeType = aDecoder.decodeObject(forKey: "dataChangeType") as? String
         isLogged = aDecoder.decodeObject(forKey: "isLogged") as? Bool
         isPersistent = aDecoder.decodeObject(forKey: "isPersistent") as? Bool
         isReadOnly = aDecoder.decodeObject(forKey: "isReadOnly") as? Bool
         dataChangeThreshold = aDecoder.decodeObject(forKey: "dataChangeThreshold") as? Int
         isBuiltIn = aDecoder.decodeObject(forKey: "isBuiltIn") as? Bool
         defaultValue = aDecoder.decodeObject(forKey: "defaultValue") as? Int
         tagType = aDecoder.decodeObject(forKey: "tagType") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if cacheTime != nil{
			aCoder.encode(cacheTime, forKey: "cacheTime")
		}
		if dataChangeType != nil{
			aCoder.encode(dataChangeType, forKey: "dataChangeType")
		}
		if isLogged != nil{
			aCoder.encode(isLogged, forKey: "isLogged")
		}
		if isPersistent != nil{
			aCoder.encode(isPersistent, forKey: "isPersistent")
		}
		if isReadOnly != nil{
			aCoder.encode(isReadOnly, forKey: "isReadOnly")
		}
		if dataChangeThreshold != nil{
			aCoder.encode(dataChangeThreshold, forKey: "dataChangeThreshold")
		}
		if isBuiltIn != nil{
			aCoder.encode(isBuiltIn, forKey: "isBuiltIn")
		}
		if defaultValue != nil{
			aCoder.encode(defaultValue, forKey: "defaultValue")
		}
		if tagType != nil{
			aCoder.encode(tagType, forKey: "tagType")
		}

	}

}