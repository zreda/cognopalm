//
//	Row.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Row : NSObject, NSCoding, Mappable{

	var infestedTreeIDs : String?
	var numberofInfestedTrees : Int?
	var totalNumberofTrees : Int?
	var descriptionField : String?
	var infestationtime : Int?
	var infested : String?
	var location : InfestedTreeID?
	var name : String?
	var severity : String?
	var tags : [AnyObject]?
	var thingTemplate : String?


	class func newInstance(map: Map) -> Mappable?{
		return Row()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		infestedTreeIDs <- map["InfestedTreeIDs"]
		numberofInfestedTrees <- map["NumberofInfestedTrees"]
		totalNumberofTrees <- map["TotalNumberofTrees"]
		descriptionField <- map["description"]
		infestationtime <- map["infestationtime"]
		infested <- map["infested"]
		location <- map["location"]
		name <- map["name"]
		severity <- map["severity"]
		tags <- map["tags"]
		thingTemplate <- map["thingTemplate"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         infestedTreeIDs = aDecoder.decodeObject(forKey: "InfestedTreeIDs") as? String
         numberofInfestedTrees = aDecoder.decodeObject(forKey: "NumberofInfestedTrees") as? Int
         totalNumberofTrees = aDecoder.decodeObject(forKey: "TotalNumberofTrees") as? Int
         descriptionField = aDecoder.decodeObject(forKey: "description") as? String
         infestationtime = aDecoder.decodeObject(forKey: "infestationtime") as? Int
         infested = aDecoder.decodeObject(forKey: "infested") as? String
         location = aDecoder.decodeObject(forKey: "location") as? InfestedTreeID
         name = aDecoder.decodeObject(forKey: "name") as? String
         severity = aDecoder.decodeObject(forKey: "severity") as? String
         tags = aDecoder.decodeObject(forKey: "tags") as? [AnyObject]
         thingTemplate = aDecoder.decodeObject(forKey: "thingTemplate") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if infestedTreeIDs != nil{
			aCoder.encode(infestedTreeIDs, forKey: "InfestedTreeIDs")
		}
		if numberofInfestedTrees != nil{
			aCoder.encode(numberofInfestedTrees, forKey: "NumberofInfestedTrees")
		}
		if totalNumberofTrees != nil{
			aCoder.encode(totalNumberofTrees, forKey: "TotalNumberofTrees")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if infestationtime != nil{
			aCoder.encode(infestationtime, forKey: "infestationtime")
		}
		if infested != nil{
			aCoder.encode(infested, forKey: "infested")
		}
		if location != nil{
			aCoder.encode(location, forKey: "location")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if severity != nil{
			aCoder.encode(severity, forKey: "severity")
		}
		if tags != nil{
			aCoder.encode(tags, forKey: "tags")
		}
		if thingTemplate != nil{
			aCoder.encode(thingTemplate, forKey: "thingTemplate")
		}

	}

}
