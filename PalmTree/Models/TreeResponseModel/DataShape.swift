//
//	DataShape.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class DataShape : NSObject, NSCoding, Mappable{

	var fieldDefinitions : FieldDefinition?


	class func newInstance(map: Map) -> Mappable?{
		return DataShape()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		fieldDefinitions <- map["fieldDefinitions"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         fieldDefinitions = aDecoder.decodeObject(forKey: "fieldDefinitions") as? FieldDefinition

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if fieldDefinitions != nil{
			aCoder.encode(fieldDefinitions, forKey: "fieldDefinitions")
		}

	}

}