//
//	RootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class TreeResponse : NSObject, NSCoding, Mappable{

	var dataShape : DataShape?
	var rows : [Row]?


	class func newInstance(map: Map) -> Mappable?{
		return TreeResponse()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		dataShape <- map["dataShape"]
		rows <- map["rows"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         dataShape = aDecoder.decodeObject(forKey: "dataShape") as? DataShape
         rows = aDecoder.decodeObject(forKey: "rows") as? [Row]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if dataShape != nil{
			aCoder.encode(dataShape, forKey: "dataShape")
		}
		if rows != nil{
			aCoder.encode(rows, forKey: "rows")
		}

	}

}
