//
//	Location.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation 
import ObjectMapper


class Location : NSObject, NSCoding, Mappable{

	var elevation : Int?
	var latitude : Int?
	var longitude : Int?


	class func newInstance(map: Map) -> Mappable?{
		return Location()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		elevation <- map["elevation"]
		latitude <- map["latitude"]
		longitude <- map["longitude"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         elevation = aDecoder.decodeObject(forKey: "elevation") as? Int
         latitude = aDecoder.decodeObject(forKey: "latitude") as? Int
         longitude = aDecoder.decodeObject(forKey: "longitude") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if elevation != nil{
			aCoder.encode(elevation, forKey: "elevation")
		}
		if latitude != nil{
			aCoder.encode(latitude, forKey: "latitude")
		}
		if longitude != nil{
			aCoder.encode(longitude, forKey: "longitude")
		}

	}

}