//
//  ServiceProvider.swift
//  Intercom
//
//  Created by Zeinab Reda on 3/11/17.
//  Copyright © 2017 Zeinab Reda. All rights reserved.
//

import Alamofire
import SVProgressHUD
class ServiceProvider {
    
    func sendUrl(showActivity:Bool,method:HTTPMethod,URLString: String, withQueryStringParameters parameters: [String : AnyObject]?, withHeaders headers: [String : String]?, completionHandler completion:@escaping (_ :NSObject?,_ :NSError?) -> Void)
    {
        if showActivity
        {
            SVProgressHUD.show()
        }
        
        
        Alamofire.request(URLString, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print("Request: \(String(describing: response.request))")
            print("Headers: \(String(describing: response.request?.allHTTPHeaderFields))")
            if method == .post
            {
                print("Body Paramters: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "")")
            }
            print("Response: \(String(describing: response.result.value))")
            
            print("Error: \(String(describing: response.error))")
            
            switch(response.result) {
            case .success(_):
                
                if response.response?.statusCode == 200
                {
                    if let data = response.result.value
                    {
                        completion(data as? NSObject ,nil)
                        
                    }
                }
               
                else if response.response?.statusCode == Constants.StatusCode.StatusNotfound
                {
                    
                    Helper.showFloatAlert(title: "No Data Found", subTitle: "", type: Constants.AlertType.AlertError)

                }
                 
                else if response.response?.statusCode == Constants.StatusCode.UserNotAuthorized
                {
                    completion(NSObject() ,NSError(domain: "User Not Found", code: (response.response?.statusCode)! , userInfo: [:]))
                    
                }
                else
                {
                    Helper.showFloatAlert(title: "Server Error , please try again later", subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                SVProgressHUD.dismiss()
                
                break
                
            case .failure(_):
                
                 if response.response?.statusCode == Constants.StatusCode.StatusOK 
                {
                    completion(NSObject() ,nil)
                    
                }
                 else if response.response?.statusCode == Constants.StatusCode.StatusNotfound {
                    completion(nil,NSError(domain: "Bad Request", code: Constants.StatusCode.StatusNotfound, userInfo: [:]))
                 }
                 else if response.response?.statusCode == Constants.StatusCode.UserNotAuthorized 
                 {
                    completion(NSObject() ,NSError(domain: "User Not Found", code: (response.response?.statusCode)! , userInfo: [:]))
                    
                    Helper.showFloatAlert(title: "User Not Found", subTitle: "", type: Constants.AlertType.AlertError)

                    
                 }
                 else
                 {
                    Helper.showFloatAlert(title: "No Internet Connection", subTitle: "", type: Constants.AlertType.AlertError)
                 }
                
                SVProgressHUD.dismiss()
                
                break
                
            }
            
        }
        
    }
    
    
    
    func sendUrlString(showActivity:Bool,method:HTTPMethod,URLString: String, withQueryStringParameters parameters: [String : AnyObject]?, withHeaders headers: [String : String]?, completionHandler completion:@escaping (_ :NSObject,_ :NSError?) -> Void)
    {
        if showActivity
        {
            SVProgressHUD.show()
        }
        
        Alamofire.request(URLString, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseString { (response) in
            print("Request: \(String(describing: response.request))")
            print("Headers: \(String(describing: response.request?.allHTTPHeaderFields))")
            print("Body Paramters: \(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue) ?? "")")
            
            print("Response: \(String(describing: response.result.value))")
            
            print("Error: \(String(describing: response.error))")
            
            switch(response.result) {
            case .success(_):
                if response.response?.statusCode == Constants.StatusCode.StatusOK
                {
                    
                    if let data = response.result.value{
                        completion(data as! NSObject ,nil)
                        
                    }
                }
                else if response.response?.statusCode == Constants.StatusCode.StatusNotfound
                {
                    Helper.showFloatAlert(title: "Server Error , please try again", subTitle: "", type: Constants.AlertType.AlertError)

                }
                else if response.response?.statusCode == Constants.StatusCode.UserNotAuthorized
                {
                    Helper.showFloatAlert(title: "User Not Found", subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                else
                {
                    Helper.showFloatAlert(title: "Server Error , please try again", subTitle: "", type: Constants.AlertType.AlertError)
                    
                }
                SVProgressHUD.dismiss()
                
                break
                
            case .failure(_):
                
                Helper.showFloatAlert(title: "No Internet Connection", subTitle: "", type: Constants.AlertType.AlertError)
                
                SVProgressHUD.dismiss()
                
                break
                
            }
            
        }
        
    }
    
   
    
    
}

