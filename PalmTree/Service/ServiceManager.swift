//
//  ServiceManager.swift
//  PalmTree
//
//  Created by Zeinab Reda on 12/30/17.
//  Copyright © 2017 Orange. All rights reserved.
//

import Foundation
class ServiceManager:NSObject {
    
    
    func getInfectedTrees(treeName:String? = "PalmTree",completion:  @escaping (_ :TreeResponse?,_ :NSError?)->Void)
    {
        
        ServiceProvider().sendUrl(showActivity: true, method: .get, URLString: String(format:Constants.main_url,treeName!), withQueryStringParameters: nil, withHeaders: ["Accept":"application/json","appKey":"cf3ac4df-8f8a-4af3-8b22-0cac7219658c"]) { (response, error) in
            
            if error == nil
            {
                completion(TreeResponse(JSON:response as! [String:Any]), error)
                
            }
            else
            {
                completion(nil, error)
                
            }
        }
    }
    
}
